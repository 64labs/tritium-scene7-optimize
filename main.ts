# Custom func for doing scene7 url optimization
# REFERENCE: http://help.adobe.com/en_US/scene7/using/WS249d0d693fdd1490-5931ca1b142b5608194-8000.html
#            http://help.adobe.com/en_US/scene7/using/WS249d0d693fdd1490-56d3b060142b593ac1c-8000.html
# Suggested best per docs - fmt=jpg&qlt=85,0&resMode=sharp2&op_usm=1.75,0.3,2,0
@func XMLNode.scene7_optimize(Text %attr_to_mod) {
    # qlt - jpeg quality setting. integer or string if passing chroma param too ('80,0').
    # wid - resulting image width. integer.
    # hei - resulting image height. integer.
    # fmt - image format (jpg,png,gif,etc).
    # op_sharpen - simple sharpening. integer or string if passing more than 1 of 3 params
    # resMode - controls the algorithm used for downsampling.
    # jpegSize - <size_in_kilobytes> - max image size
    # fit=constrain,1

    $format = 'jpg'
    $quality = '85,0'
    $resample_mode = 'sharp2'
    $unsharp_mask = '1.75,0.3,2,0'
    $width = ''
    $height = ''
    $sharpen = ''
    $jpeg_size = ''
    $fit = ''

    yield()

    $src = fetch('@' + %attr_to_mod)

    $src = url($src) {
        remove_param('fmt')
        param('fmt', $format)

        remove_param('qlt')
        param('qlt', $quality)

        remove_param('resMode')
        param('resMode', $resample_mode)

        remove_param('op_usm')
        param('op_usm', $unsharp_mask)

        match_not($width, '') {
            remove_param('wid')
            param('wid', $width)
        }
        match_not($height, '') {
            remove_param('hei')
            param('hei', $height)
        }
        match_not($sharpen, '') {
            remove_param('op_sharpen')
            param('op_sharpen', $sharpen)
        }
        match_not($jpeg_size, '') {
            remove_param('jpegSize')
            param('jpegSize', $jpeg_size)
        }
        match_not($fit, '') {
            remove_param('fit')
            param('fit', $fit)
        }
    }

    attribute('src', $src)
}

@func XMLNode.scene7_optimize() {
    scene7_optimize('src') {
        yield()
    }
}
