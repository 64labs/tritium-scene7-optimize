# Basics
Custom func for doing scene7 url optimization

# References
* [Best practices for optimizing the quality of your images](http://help.adobe.com/en_US/scene7/using/WS249d0d693fdd1490-5931ca1b142b5608194-8000.html)
* [Best practices summary](http://help.adobe.com/en_US/scene7/using/WS249d0d693fdd1490-56d3b060142b593ac1c-8000.html)
* Suggested best per docs - fmt=jpg&qlt=85,0&resMode=sharp2&op_usm=1.75,0.3,2,0

# Overview

###XMLNode.scene7_optimize(Text %attribute_name)

- modifies an image's attribute to optimize the image
- allows customization of url using adobe's scene7 image optimization service

**options:**

* qlt - jpeg quality setting. integer or string if passing chroma param too ('80,0')
* wid - resulting image width. integer
* hei - resulting image height. integer
* fmt - image format (jpg,png,gif,etc)
* op_sharpen - simple sharpening. integer or string if passing more than 1 of 3 params
* resMode - controls the algorithm used for downsampling
* jpegSize - <size_in_kilobytes> - max image size
* fit - 'constrain,1'


**defaults:**

* $format = 'jpg'
* $quality = '85,0'
* $resample_mode = 'sharp2'
* $unsharp_mask = '1.75,0.3,2,0'
* $width = ''
* $height = ''
* $sharpen = ''
* $jpeg_size = ''
* $fit = ''


**example:**
```
#!
html() {
    $("//img") {
        scene7_optimize('data-src') {
            $quality = 75
            $width = 125
        }
    }
}
```

###XMLNode.scene7_optimize()

- overload function, making %attribute_name optional
- calls "scene7_optimize" and defaults attribute_name to 'src'

**example:**

```
#!
html() {
    $("//img") {
        scene7_optimize() {
            $quality = 75
            $width = 125
        }
    }
}
```
